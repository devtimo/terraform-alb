module "alb" {
    source  = "terraform-aws-modules/alb/aws"

    version = "~> 5.0"

    name = var.name
    load_balancer_type = "application"

    vpc_id             = var.vpc_id
    subnets            = var.subnets
    security_groups    = var.security_groups

    # access_logs = {
    #     bucket = "my-alb-logs"
    # }

    target_groups = [
        {
            backend_protocol = "HTTP"
            backend_port     = 80
            target_type      = "instance"
        }
    ]

    http_tcp_listeners = [
        {
            port               = 80
            protocol           = "HTTP"
            target_group_index = 0
        }
    ]
}