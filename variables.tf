variable "name" {
  description = "Nome do Application Load Balancer"
}


variable "vpc_id" {
  description = "VPC ID"
}

variable "subnets" {
  type = list(string)
  description = "IDs das subnets"
}

variable "security_groups" {
  type = list(string)
  description = "Security groups"
}


