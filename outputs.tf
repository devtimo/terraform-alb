output "http_listener" {
  value = module.alb.http_tcp_listener_arns[0]
}

output "zone_id" {
  value = module.alb.this_lb_zone_id
}

output "dns_name" {
  value = module.alb.this_lb_dns_name
}

